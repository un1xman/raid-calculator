number = int(input("Least two disks are required: "))
size = int(input("Enter single disk size, GB: "))
#number = int(numb)
#disk_size = int(size)
#print(type(number))
#print(type(size))
print("Choice RAID type")
print("=========================")
print("1) RAID 0 (Stripe set)")
print("2) RAID 1 (Mirror)")
print("3) RAID 5 (Parity)")
print("4) RAID 5E (Parity+spare")
print("5) RAID 10 (Mirror+stripe)")
print("6) RAID 6 (double parity)")
rtype = int(input("Enter here type: "))
if rtype == 1:
    result = number * size
    print("Your raid configuration capacity is " + str(result) + "GB")
elif rtype == 2:
    result = size
    print("Your raid configuration capacity is " + str(result) + "GB")
elif rtype == 3:
    if number < 3:
        print("Uups Minimum of 3 drives is required for RAID 5")
    else:
        result = (number - 1) * size
        print("Your raid configuration capacity is " + str(result) + "GB")
elif rtype == 4:
    if number < 4:
        print("Uups Minimum of 4 drives is required for RAID 5E")
    else:
        result = (number - 2) * size
        print("Your raid configuration capacity is " + str(result) + "GB")
elif rtype == 5:
    if number < 4:
        print("Uups Minimum of 4 drives is required for RAID 10")
    else:
        if number % 2 == 0:
            result = (number / 2) * size
            print("Your raid configuration capacity is " + str(result) + "GB")
        else:
            result = ((number - 1) / 2) * size
            print("Your raid configuration capacity is " + str(result) + "GB")
elif rtype == 5:
    if number < 4:
        print("Uups Minimum of 4 drives is required for RAID 6")
    else:
        result = (number - 2) * size
        print("Your raid configuration capacity is " + str(result) + "GB")

